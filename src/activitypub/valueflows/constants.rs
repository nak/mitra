// https://www.valueflo.ws/concepts/actions/#action-definitions
pub const ACTION_DELIVER_SERVICE: &str = "deliverService";
pub const ACTION_TRANSFER: &str = "transfer";

pub const CLASS_CONTENT: &str = "https://www.wikidata.org/wiki/Q1260632";

// http://www.ontology-of-units-of-measure.org/resource/om-2/one
pub const UNIT_ONE: &str = "one";
// http://www.ontology-of-units-of-measure.org/resource/om-2/second-Time
pub const UNIT_SECOND: &str = "second";
