use actix_web::HttpRequest;
use serde_json::Value;

use mitra_config::Config;
use mitra_models::database::{DatabaseClient, DatabaseError};
use mitra_utils::urls::get_hostname;
use mitra_validators::errors::ValidationError;

use crate::errors::HttpError;

use super::authentication::{
    verify_signed_activity,
    verify_signed_request,
    AuthenticationError,
};
use super::deserialization::find_object_id;
use super::fetcher::fetchers::FetchError;
use super::handlers::{
    accept::handle_accept,
    add::handle_add,
    announce::handle_announce,
    create::{
        handle_create,
        validate_create,
    },
    delete::handle_delete,
    follow::handle_follow,
    like::handle_like,
    r#move::handle_move,
    reject::handle_reject,
    remove::handle_remove,
    undo::handle_undo,
    update::handle_update,
};
use super::identifiers::profile_actor_id;
use super::queues::IncomingActivityJobData;
use super::vocabulary::*;

#[derive(thiserror::Error, Debug)]
pub enum HandlerError {
    #[error("local object")]
    LocalObject,

    #[error(transparent)]
    FetchError(#[from] FetchError),

    #[error(transparent)]
    ValidationError(#[from] ValidationError),

    #[error(transparent)]
    DatabaseError(#[from] DatabaseError),

    #[error(transparent)]
    AuthError(#[from] AuthenticationError),
}

impl From<HandlerError> for HttpError {
    fn from(error: HandlerError) -> Self {
        match error {
            HandlerError::LocalObject => HttpError::InternalError,
            HandlerError::FetchError(error) => {
                HttpError::ValidationError(error.to_string())
            },
            HandlerError::ValidationError(error) => error.into(),
            HandlerError::DatabaseError(error) => error.into(),
            HandlerError::AuthError(_) => {
                HttpError::AuthError("invalid signature")
            },
        }
    }
}

pub async fn handle_activity(
    config: &Config,
    db_client: &mut impl DatabaseClient,
    activity: &Value,
    is_authenticated: bool,
) -> Result<(), HandlerError> {
    let activity_type = activity["type"].as_str()
        .ok_or(ValidationError("type property is missing"))?
        .to_owned();
    let activity_actor = find_object_id(&activity["actor"])
        .map_err(|_| ValidationError("invalid actor property"))?;
    let activity = activity.clone();
    let maybe_object_type = match activity_type.as_str() {
        ACCEPT => {
            handle_accept(config, db_client, activity).await?
        },
        ADD => {
            handle_add(config, db_client, activity).await?
        },
        ANNOUNCE => {
            handle_announce(config, db_client, activity).await?
        },
        CREATE => {
            handle_create(config, db_client, activity, is_authenticated).await?
        },
        DELETE => {
            handle_delete(config, db_client, activity).await?
        },
        FOLLOW => {
            handle_follow(config, db_client, activity).await?
        },
        LIKE | EMOJI_REACT => {
            handle_like(config, db_client, activity).await?
        },
        MOVE => {
            handle_move(config, db_client, activity).await?
        },
        REJECT => {
            handle_reject(config, db_client, activity).await?
        },
        REMOVE => {
            handle_remove(config, db_client, activity).await?
        },
        UNDO => {
            handle_undo(config, db_client, activity).await?
        },
        UPDATE => {
            handle_update(config, db_client, activity).await?
        },
        _ => {
            log::warn!("activity type is not supported: {}", activity);
            None
        },
    };
    if let Some(object_type) = maybe_object_type {
        log::info!(
            "processed {}({}) from {}",
            activity_type,
            object_type,
            activity_actor,
        );
    };
    Ok(())
}

pub async fn receive_activity(
    config: &Config,
    db_client: &mut impl DatabaseClient,
    request: &HttpRequest,
    activity: &Value,
) -> Result<(), HandlerError> {
    let activity_type = activity["type"].as_str()
        .ok_or(ValidationError("type property is missing"))?;
    let activity_actor = find_object_id(&activity["actor"])
        .map_err(|_| ValidationError("invalid actor property"))?;

    let actor_hostname = get_hostname(&activity_actor)
        .map_err(|_| ValidationError("invalid actor ID"))?;
    if config.blocked_instances.iter()
        .any(|instance_hostname| &actor_hostname == instance_hostname)
    {
        log::warn!("ignoring activity from blocked instance: {}", activity);
        return Ok(());
    };

    let is_self_delete = if activity_type == DELETE {
        let object_id = find_object_id(&activity["object"])?;
        object_id == activity_actor
    } else { false };

    // HTTP signature is required
    let mut signer = match verify_signed_request(
        config,
        db_client,
        request,
        // Don't fetch signer if this is Delete(Person) activity
        is_self_delete,
    ).await {
        Ok(request_signer) => {
            log::debug!("request signed by {}", request_signer.acct);
            request_signer
        },
        Err(error) => {
            if is_self_delete && matches!(
                error,
                AuthenticationError::NoHttpSignature |
                AuthenticationError::DatabaseError(DatabaseError::NotFound(_))
            ) {
                // Ignore Delete(Person) activities without HTTP signatures
                // or if signer is not found in local database
                return Ok(());
            };
            log::warn!("invalid HTTP signature: {}", error);
            return Err(error.into());
        },
    };

    // JSON signature is optional
    match verify_signed_activity(
        config,
        db_client,
        activity,
        // Don't fetch actor if this is Delete(Person) activity
        is_self_delete,
    ).await {
        Ok(activity_signer) => {
            if activity_signer.acct != signer.acct {
                log::warn!(
                    "request signer {} is different from activity signer {}",
                    signer.acct,
                    activity_signer.acct,
                );
            } else {
                log::debug!("activity signed by {}", activity_signer.acct);
            };
            // Activity signature has higher priority
            signer = activity_signer;
        },
        Err(AuthenticationError::NoJsonSignature) => (), // ignore
        Err(other_error) => {
            log::warn!("invalid JSON signature: {}", other_error);
        },
    };

    if config.blocked_instances.iter()
        .any(|instance| signer.hostname.as_ref() == Some(instance))
    {
        log::warn!("ignoring activity from blocked instance: {}", activity);
        return Ok(());
    };

    let signer_id = profile_actor_id(&config.instance_url(), &signer);
    let is_authenticated = activity_actor == signer_id;
    if !is_authenticated {
        match activity_type {
            CREATE => (), // Accept forwarded Create() activities
            DELETE | LIKE => {
                // Ignore forwarded Delete and Like activities
                return Ok(());
            },
            _ => {
                // Reject other types
                log::warn!(
                    "request signer {} does not match actor {}",
                    signer_id,
                    activity_actor,
                );
                return Err(AuthenticationError::UnexpectedSigner.into());
            },
        };
    };

    if activity_type == CREATE {
        // Validate before putting into the queue
        validate_create(config, db_client, activity).await?;
    };

    if let ANNOUNCE | CREATE | DELETE | MOVE | UNDO | UPDATE = activity_type {
        // Add activity to job queue and release lock
        IncomingActivityJobData::new(activity, is_authenticated)
            .into_job(db_client, 0).await?;
        log::debug!("activity added to the queue: {}", activity_type);
        return Ok(());
    };

    handle_activity(
        config,
        db_client,
        activity,
        is_authenticated,
    ).await
}
