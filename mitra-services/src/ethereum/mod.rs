mod api;
pub mod contracts;
pub mod eip4361;
mod errors;
pub mod gate;
pub mod signatures;
pub mod subscriptions;
pub mod sync;
pub mod utils;

#[cfg(feature = "ethereum-extras")]
pub mod erc721_metadata;
#[cfg(feature = "ethereum-extras")]
pub mod nft;

pub use api::{EthereumApi, EthereumContract};
pub use errors::EthereumError;
