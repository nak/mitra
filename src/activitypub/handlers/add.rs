use serde::Deserialize;
use serde_json::Value;

use mitra_config::Config;
use mitra_models::{
    database::{DatabaseClient, DatabaseError},
    posts::queries::{
        get_post_by_remote_object_id,
        set_pinned_flag,
    },
    profiles::queries::get_profile_by_remote_actor_id,
    relationships::queries::subscribe_opt,
    users::queries::get_user_by_name,
};
use mitra_validators::errors::ValidationError;

use crate::activitypub::{
    identifiers::parse_local_actor_id,
    vocabulary::{NOTE, PERSON},
};

use super::{HandlerError, HandlerResult};

#[derive(Deserialize)]
struct Add {
    actor: String,
    object: String,
    target: String,
}

pub async fn handle_add(
    config: &Config,
    db_client: &mut impl DatabaseClient,
    activity: Value,
) -> HandlerResult {
    let activity: Add = serde_json::from_value(activity)
        .map_err(|_| ValidationError("unexpected activity structure"))?;
    let actor_profile = get_profile_by_remote_actor_id(
        db_client,
        &activity.actor,
    ).await?;
    let actor = actor_profile.actor_json.ok_or(HandlerError::LocalObject)?;
    if Some(activity.target.clone()) == actor.subscribers {
        // Adding to subscribers
        let username = parse_local_actor_id(
            &config.instance_url(),
            &activity.object,
        )?;
        let user = get_user_by_name(db_client, &username).await?;
        subscribe_opt(db_client, &user.id, &actor_profile.id).await?;
        return Ok(Some(PERSON));
    };
    if Some(activity.target) == actor.featured {
        // Add to featured
        let post = match get_post_by_remote_object_id(
            db_client,
            &activity.object,
        ).await {
            Ok(post) => post,
            Err(DatabaseError::NotFound(_)) => return Ok(None),
            Err(other_error) => return Err(other_error.into()),
        };
        set_pinned_flag(db_client, &post.id, true).await?;
        return Ok(Some(NOTE));
    };
    Ok(None)
}
