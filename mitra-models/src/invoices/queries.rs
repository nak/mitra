use uuid::Uuid;

use mitra_utils::{
    caip2::ChainId,
    id::generate_ulid,
};

use crate::database::{
    catch_unique_violation,
    DatabaseClient,
    DatabaseError,
    DatabaseTypeError,
};

use super::types::{DbChainId, DbInvoice, InvoiceStatus};

pub async fn create_invoice(
    db_client: &impl DatabaseClient,
    sender_id: &Uuid,
    recipient_id: &Uuid,
    chain_id: &ChainId,
    payment_address: &str,
    amount: i64,
) -> Result<DbInvoice, DatabaseError> {
    let invoice_id = generate_ulid();
    let row = db_client.query_one(
        "
        INSERT INTO invoice (
            id,
            sender_id,
            recipient_id,
            chain_id,
            payment_address,
            amount
        )
        VALUES ($1, $2, $3, $4, $5, $6)
        RETURNING invoice
        ",
        &[
            &invoice_id,
            &sender_id,
            &recipient_id,
            &DbChainId::new(chain_id),
            &payment_address,
            &amount,
        ],
    ).await.map_err(catch_unique_violation("invoice"))?;
    let invoice = row.try_get("invoice")?;
    Ok(invoice)
}

pub async fn get_invoice_by_id(
    db_client: &impl DatabaseClient,
    invoice_id: &Uuid,
) -> Result<DbInvoice, DatabaseError> {
    let maybe_row = db_client.query_opt(
        "
        SELECT invoice
        FROM invoice WHERE id = $1
        ",
        &[&invoice_id],
    ).await?;
    let row = maybe_row.ok_or(DatabaseError::NotFound("invoice"))?;
    let invoice = row.try_get("invoice")?;
    Ok(invoice)
}

pub async fn get_invoice_by_address(
    db_client: &impl DatabaseClient,
    chain_id: &ChainId,
    payment_address: &str,
) -> Result<DbInvoice, DatabaseError> {
    let maybe_row = db_client.query_opt(
        "
        SELECT invoice
        FROM invoice WHERE chain_id = $1 AND payment_address = $2
        ",
        &[&DbChainId::new(chain_id), &payment_address],
    ).await?;
    let row = maybe_row.ok_or(DatabaseError::NotFound("invoice"))?;
    let invoice = row.try_get("invoice")?;
    Ok(invoice)
}

pub async fn get_invoice_by_participants(
    db_client: &impl DatabaseClient,
    sender_id: &Uuid,
    recipient_id: &Uuid,
    chain_id: &ChainId,
) -> Result<DbInvoice, DatabaseError> {
    // Always return oldest invoice
    let maybe_row = db_client.query_opt(
        "
        SELECT invoice
        FROM invoice
        WHERE
            sender_id = $1
            AND recipient_id = $2
            AND chain_id = $3
        ORDER BY created_at DESC
        LIMIT 1
        ",
        &[
            &sender_id,
            &recipient_id,
            &DbChainId::new(chain_id),
        ],
    ).await?;
    let row = maybe_row.ok_or(DatabaseError::NotFound("invoice"))?;
    let invoice = row.try_get("invoice")?;
    Ok(invoice)
}

pub async fn get_invoices_by_status(
    db_client: &impl DatabaseClient,
    chain_id: &ChainId,
    status: InvoiceStatus,
) -> Result<Vec<DbInvoice>, DatabaseError> {
    let rows = db_client.query(
        "
        SELECT invoice
        FROM invoice WHERE chain_id = $1 AND invoice_status = $2
        ",
        &[&DbChainId::new(chain_id), &status],
    ).await?;
    let invoices = rows.iter()
        .map(|row| row.try_get("invoice"))
        .collect::<Result<_, _>>()?;
    Ok(invoices)
}

pub async fn set_invoice_status(
    db_client: &mut impl DatabaseClient,
    invoice_id: &Uuid,
    new_status: InvoiceStatus,
) -> Result<DbInvoice, DatabaseError> {
    let transaction = db_client.transaction().await?;
    let maybe_row = transaction.query_opt(
        "
        SELECT invoice_status
        FROM invoice WHERE id = $1
        FOR UPDATE
        ",
        &[&invoice_id],
    ).await?;
    let row = maybe_row.ok_or(DatabaseError::NotFound("invoice"))?;
    let old_status: InvoiceStatus = row.try_get("invoice_status")?;
    if !old_status.can_change(&new_status) {
        return Err(DatabaseTypeError.into());
    };
    let maybe_row = transaction.query_opt(
        "
        UPDATE invoice
        SET
            invoice_status = $2,
            updated_at = CURRENT_TIMESTAMP
        WHERE id = $1
        RETURNING invoice
        ",
        &[&invoice_id, &new_status],
    ).await?;
    let row = maybe_row.ok_or(DatabaseError::NotFound("invoice"))?;
    let invoice = row.try_get("invoice")?;
    transaction.commit().await?;
    Ok(invoice)
}

pub async fn set_invoice_payout_tx_id(
    db_client: &impl DatabaseClient,
    invoice_id: &Uuid,
    payout_tx_id: Option<&str>,
) -> Result<DbInvoice, DatabaseError> {
    let maybe_row = db_client.query_opt(
        "
        UPDATE invoice SET payout_tx_id = $2
        WHERE id = $1
        RETURNING invoice
        ",
        &[&invoice_id, &payout_tx_id],
    ).await?;
    let row = maybe_row.ok_or(DatabaseError::NotFound("invoice"))?;
    let invoice = row.try_get("invoice")?;
    Ok(invoice)
}

#[cfg(test)]
mod tests {
    use serial_test::serial;
    use crate::database::test_utils::create_test_database;
    use crate::profiles::{
        queries::create_profile,
        types::ProfileCreateData,
    };
    use crate::users::{
        queries::create_user,
        types::UserCreateData,
    };
    use super::*;

    async fn create_sender_and_recipient(
        db_client: &mut impl DatabaseClient,
    ) -> (Uuid, Uuid) {
        let sender_data = ProfileCreateData {
            username: "sender".to_string(),
            ..Default::default()
        };
        let sender = create_profile(db_client, sender_data).await.unwrap();
        let recipient_data = UserCreateData {
            username: "recipient".to_string(),
            password_hash: Some("test".to_string()),
            ..Default::default()
        };
        let recipient = create_user(db_client, recipient_data).await.unwrap();
        (sender.id, recipient.id)
    }

    #[tokio::test]
    #[serial]
    async fn test_create_invoice() {
        let db_client = &mut create_test_database().await;
        let (sender_id, recipient_id) =
            create_sender_and_recipient(db_client).await;
        let chain_id = ChainId::monero_mainnet();
        let payment_address = "8MxABajuo71BZya9";
        let amount = 100000000000109212;
        let invoice = create_invoice(
            db_client,
            &sender_id,
            &recipient_id,
            &chain_id,
            payment_address,
            amount,
        ).await.unwrap();
        assert_eq!(invoice.sender_id, sender_id);
        assert_eq!(invoice.recipient_id, recipient_id);
        assert_eq!(invoice.chain_id.into_inner(), chain_id);
        assert_eq!(invoice.payment_address, payment_address);
        assert_eq!(invoice.amount, amount);
        assert_eq!(invoice.invoice_status, InvoiceStatus::Open);
        assert_eq!(invoice.payout_tx_id, None);
        assert_eq!(invoice.updated_at, invoice.created_at);
    }

    #[tokio::test]
    #[serial]
    async fn test_set_invoice_status() {
        let db_client = &mut create_test_database().await;
        let (sender_id, recipient_id) =
            create_sender_and_recipient(db_client).await;
        let invoice = create_invoice(
            db_client,
            &sender_id,
            &recipient_id,
            &ChainId::monero_mainnet(),
            "8MxABajuo71BZya9",
            100000000000000,
        ).await.unwrap();

        let invoice = set_invoice_status(
            db_client,
            &invoice.id,
            InvoiceStatus::Paid,
        ).await.unwrap();
        assert_eq!(invoice.invoice_status, InvoiceStatus::Paid);
        assert_ne!(invoice.updated_at, invoice.created_at);

        let error = set_invoice_status(
            db_client,
            &invoice.id,
            InvoiceStatus::Cancelled,
        ).await.err().unwrap();
        assert!(matches!(error, DatabaseError::DatabaseTypeError(_)));
    }
}
