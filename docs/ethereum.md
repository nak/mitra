# Ethereum integration

Install Ethereum client or choose a JSON-RPC API provider.

Deploy contracts on the blockchain. Instructions can be found at https://codeberg.org/silverpill/mitra-contracts.

Add blockchain configuration to `blockchains` array in your configuration file.
